FROM gitlab-registry.cern.ch/linuxsupport/cs8-base

ARG ARCH
ARG BUILD_TIME
ARG COMMIT_SHORT_SHA
ENV COMMIT_SHORT_SHA=$COMMIT_SHORT_SHA

RUN echo "Running on '${ARCH}' with commit '${COMMIT_SHORT_SHA}', built at ${BUILD_TIME}."
